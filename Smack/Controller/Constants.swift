//
//  Constants.swift
//  Smack
//
//  Created by aya reda on 10/1/17.
//  Copyright © 2017 aya reda. All rights reserved.
//

import Foundation
typealias CompletionHandler = (_ Success:Bool) ->()
//URLS CONSTANTS
let BASE_URL="https://chattychataya.herokuapp.com/v1/"
let URL_REGISTER="\(BASE_URL)/account/register"
let URL_LOGIN = "\(BASE_URL)/account/login"

// Segues
let TO_LOGIN = "toLogin"
let TO_CREAT_ACCOUNT = "toCreatAccount"

//User Defaults

let TOKEN_KEY="token"
let LOGGED_IN_KEY = "loggedIn"
let USER_EMAIL = "userEmail"

// HEADERS
 let  HEADER = ["Content_type": "application/json;charset=utf-8"]


